
//********************************************************************
//File:         Driver.java       
//Author:       Liam Quinn
//Date:         November 7th 2017
//Course:       CPS100
//
//Problem Statement:
// Write a Java interface called Priority that includes two methods: 
// setPriority and getPriority. The interface should define a way to
// establish numeric property among a set of objects. 
// Design and implement a class called Task that represents a task
// (such as on a to-do list) that implements the Priority interface.
// Create a driver class to exercise some Task objects.
// 
//Inputs: 
//Outputs:  
//  
//********************************************************************

import java.util.*;


public class Driver
{

  public static void main(String[] args)
  {
    Scanner scan = new Scanner(System.in);

    System.out.println("Input task#1: ");
    String task = scan.nextLine();
    Task tasker = new Task(task);

    System.out.println("Set priority of task: ");
    int taskPriority = scan.nextInt();
    tasker.setPriority(taskPriority);

    System.out.println(tasker.toString());

    scan.close();


  }

}
