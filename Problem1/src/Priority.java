public interface Priority
{
  public abstract int getPriority();

  public abstract void setPriority(int priority);
}
