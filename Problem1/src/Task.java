
public class Task implements Priority
{
  public int priority;
  public String description;
  private String task = "";

  // create constructor
  public Task(String task)
  {
    this.task = task;
  }

  public int getPriority()
  {
    return this.priority;
  }

  public void setPriority(int priority)
  {
    this.priority = priority;
  }


  // create toString
  public String toString()
  {
    String str = "";
    str += "Priority of \"" + task + "\" is set to: " + priority;

    return str;
  }
}
