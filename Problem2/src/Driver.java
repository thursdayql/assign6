
//********************************************************************
//File:         Driver.java       
//Author:       Liam Quinn
//Date:         November 7th 2017
//Course:       CPS100
//
//Problem Statement:
// Write a Java interface called Lockable that includes the following
// methods: setKey, lock, unlock, and locked. The setKey, lock, and unlock
// methods take an integer parameter that represents the key. The setKey 
// method establishes the key. The lock and unlock methods lock and unlock
// the object, but only if the key passed in is correct. The locked method
// returns a boolean that indicates whether or not the object is locked.
// A Lockable object represents an object whose regular methods are protected:
// if the object is locked, the methods cannot be invoked; if it is unlocked, 
// they can be invoked. Write a version of the Coin class from Chapter 5 so 
// that it is Lockable.
// 
//Inputs: 
//Outputs:  
// 
//********************************************************************

import java.util.*;


public class Driver
{

  public static void main(String[] args)
  {
    Scanner scan = new Scanner(System.in);
    int again = 1;
    boolean loop;
    SecureCoin coin = new SecureCoin();
    coin.setKey(12);

    do
    {
      coin.flip();

      System.out.println("Enter Key");
      int keyGuess = scan.nextInt();

      coin.unlock(keyGuess);

      if (!coin.locked())
      {
        System.out.println("Correct Key");
        System.out.println();
        System.out.println();

        coin.flip();
        coin.toString();
        System.out.println();
      }
      else
      {
        System.out.println();
        System.out.println();
        System.out.println("Wrong Key");
        System.out.println("Coin locked - NO action");
      }

      if (!coin.locked())
      {
        System.out.println("Set New Key (INTEGER)");
        int newKey = scan.nextInt();

        System.out.println("Relocking key...");
        System.out.println();
        coin.setKey(newKey);
        coin.lock(newKey);
      }

      System.out.println("Go again? (Yes = 1, No = Any other Integer)");
      again = scan.nextInt();

      if (again == 1)
      {
        loop = true;
        System.out.println();
      }
      else
        loop = false;

    }
    while (loop == true);
  }

}
