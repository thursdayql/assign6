public class SecureCoin implements Lockable
{
  private int key = -1;

  private boolean locked;
  private final int HEADS = 0;
  private final int TAILS = 1;

  private int face;

  //-----------------------------------------------------------------
  //  Sets up the coin by flipping it initially.
  //-----------------------------------------------------------------
  public SecureCoin()
  {
    flip();
  }

  //-----------------------------------------------------------------
  //  Flips the coin by randomly choosing a face value.
  //-----------------------------------------------------------------
  public void flip()
  {
    if (!this.locked())
    {
      face = (int) (Math.random() * 2);
    }
    else
      System.out.println("Coin is Locked - NO Action");
  }

  public void setKey(int key)
  {
    this.key = key;
    this.locked = true;
  }

  public boolean lock(int key)
  {
    if (key == this.key)
    {
      this.locked = true;
      return true;
    }
    else
      return false;
  }

  public boolean unlock(int key)
  {
    if (key == this.key)
    {
      this.locked = false;
      return true;
    }
    else
      return false;
  }

  public boolean locked()
  {
    return this.locked;
  }

  public String toString()
  {
    if (!this.locked)
    {
      String faceName;

      if (face == HEADS)
        faceName = "Heads";
      else
        faceName = "Tails";

      System.out.println("Your coin flipped: " + faceName);

      return faceName;
    }
    else
      return "Locked - NO action";
  }
}
