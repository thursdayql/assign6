
public class multiConcatClass
{

  //Overloaded Method
  public String multiConcat(String str)
  {
    String concatString = str + str;
    System.out.println("Your string concatenated with itself is: ");
    return concatString;
  }

  // Original Method
  public String multiConcat(String str, int integer)
  {
    String concatString = "";

    if (integer >= 1)
    {
      for (int i = 0; i < integer; i++)
        concatString += str;
      System.out.println("Your string concated with itself " + integer
                         + " ammount of times is: ");
    }

    return concatString;
  }
}
