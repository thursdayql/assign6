
//********************************************************************
//File:         Driver.java       
//Author:       Liam Quinn
//Date:         November 7th 2017
//Course:       CPS100
//
//Problem Statement:
// Overload the multiConcat method from exercise 7.4 such that if the
// integer parameter is  not provided, the method returns the string 
// concatenated with itself. For example, if the parameter is "test",
// the return value is "testtest".
//
// 
//Inputs: String and Integer from user
//Outputs:  String concatenated with itself (Integer) amount of times
// 
//********************************************************************

import java.util.*;


// I accidentally did this problem, but decided to include it as well.

public class Driver
{

  public static void main(String[] args)
  {
    Scanner scan = new Scanner(System.in);
    multiConcatClass root = new multiConcatClass();

    System.out.println("Give String: ");
    String str = scan.nextLine();

    System.out.println("Give Integer: ");
    int integer = scan.nextInt();

    System.out.println(root.multiConcat(str));
    System.out.println(root.multiConcat(str, integer));

    scan.close();
  }
}
